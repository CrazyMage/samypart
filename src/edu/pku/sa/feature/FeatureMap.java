package edu.pku.sa.feature;

import java.util.HashMap;

/**
 * 
 * @author intfloat@pku.edu.cn, stormier@126.com
 * 
 * Keep <feature, index> mapping relation
 *
 */
public class FeatureMap {
	
	private static HashMap<String, Integer> map;
	
	/**
	 * 
	 * @param f is a feature instance
	 * @return index of feature f
	 */
	public static int getIndex(Feature f) {
		if (null == map) 
			map = new HashMap<String, Integer>();
		
//		insert feature f into map if it doesn't exist
		if (!map.containsKey(f.getDesc()))
			map.put(f.getDesc(), map.size());
		
		return map.get(f.getDesc());
	}
	
	/**
	 * Clear current feature map
	 */
	public static void clearFeatureMap() {
		if (null == map) map = new HashMap<String, Integer>();
		else map.clear();
		return;
	}
	
	/**
	 * Print feature mapping information to console
	 */
	public static void printMapping() {
		for (String s : map.keySet()) {
			System.out.println("< " + s + ", " + map.get(s) + ">");
		}
		return;
	}

} // end class FeatureMap
