# use scikit-learn to implement logistic regression
# sparsity is a major problem
from sklearn.linear_model import SGDClassifier
from sklearn.feature_extraction import FeatureHasher
from numpy import asarray
from random import random

# Logistic Regression for sparse features using scikit-learn library
if __name__ == '__main__':
    comments = ['this is an excellent movie',
            'I do not like it.',
            'it\'s a piece of shit!',
            'fine on the whole, but not quite satisfying',
            'just so so']
    train_y = [5, 1, 1, 4, 3]
    # normalization step
    # train_y = [(y - 1.0) / 4.0 for y in train_y]
    clf = SGDClassifier(loss = 'log')

    train_x = []
    for x in comments:
        cur = {}
        for word in x.split():
            cur[word] = random()
        train_x.append(cur)
    # train_x = [x.split() for x in comments]
    vectorizer = FeatureHasher(input_type = 'dict', non_negative = True)
    train_x = vectorizer.transform(train_x)
    print 'train_x:', train_x

    clf.fit(train_x, train_y)
    print clf.predict(train_x)

