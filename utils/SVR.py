# -*- coding: utf-8 -*-
import logging
from sklearn.linear_model import SGDClassifier
from sklearn.feature_extraction import FeatureHasher
from numpy import asarray
from random import random
from sklearn import svm
from numpy.random import shuffle
from sklearn.metrics import mean_squared_error

# Support Vector Regression
if __name__ == '__main__':
    logging.basicConfig(level = logging.DEBUG,
                format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                datefmt='%a, %d %b %Y %H:%M:%S',
                filename='sa.log',
                filemode='w')

    logging.info('start to load data...')
    reader = open('../train_xy.txt', 'r')
    data = []
    for instance in reader:
        instance = instance.split()
        y = float(instance[0])
        x = {}
        for feature in instance[1:]:
            pos = feature.rfind(':')
            key, val = feature[:pos], feature[pos + 1:]
            print "key: %s val: %s\n" % (key,val)
            x[key] = float(val)
        data.append([x, y])
    logging.info('Total number of instance: ' + str(len(data)))


#   random shuffle and split data into training set & test set
    shuffle(data)
    X = [ins[0] for ins in data]
    Y = [ins[1] for ins in data]
    mid = int(len(data) * .8)
    train_x, test_x = X[:mid], X[mid:]
    train_y, test_y = Y[:mid], Y[mid:]
    logging.info('Number of train data: ' + str(len(train_x)))
    logging.info('Number of test data: ' + str(len(test_x)))


    logging.info('start to train a SVR model...')
    clf = svm.SVR()
    vectorizer = FeatureHasher(input_type = 'dict', non_negative = True)
    train_x = vectorizer.transform(train_x)
    clf.fit(train_x, train_y)

    logging.info('start to test model...')
    test_x = vectorizer.transform(test_x)
    pred_y = clf.predict(test_x)
    for y1, y2 in zip(test_y, pred_y):
        print y1, y2
    logging.info('mean_squared_error: ' + str(mean_squared_error(test_y, pred_y)))


